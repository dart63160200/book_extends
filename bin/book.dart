import 'dart:ffi';

import 'package:book/book.dart' as book;

class Novel extends Book with book_cover {
  Novel(super.name, super.page);
    @override
    String toString() {
      // TODO: implement toString
      return super.toString();
    }

    @override
    void cover() { // มีปก
      // TODO: implement cover
      super.cover();
    }
}

class Periodical extends Book {
  Periodical(super.name, super.page);
}

class newspaper extends Periodical {
  newspaper(super.name, super.page);
}

class magazine extends Periodical with book_cover {
  magazine(super.name, super.page);
  @override
  void cover() { // มีปก
    // TODO: implement cover
    super.cover();
  }
}

mixin book_cover { // ปก
  void cover() {
    print("Book cover");
  }
}

class Book { 
  String name = "";
  var page;

  Book(String name, int page) {
    this.name = name;
    this.page = page;
  }

  String get getName {
    return name;
  }

  int get getPage {
    return page;
  }

  void read() { // อ่านได้
    print(name + " read");
  }

  void write() { // เขียนได้
    print(name + " write");
  }

  void tostringNamePage() { // ดูชื่อหนังสือ เเละ จำนวนหน้าของหนังสือ
    return print("Book: " + name + " , " + "page: " + page.toString());
  }
}

void main() {
  Book book = new Book("Book", 200); // Book
  book.tostringNamePage();
  print("--------------------");
  Novel book1 = new Novel("Novel", 233); // Book Novel
  book1.tostringNamePage();
  book1.read();
  book1.write();
  print("--------------------");
  newspaper book2 = new newspaper("เดลินิวส์ ", 10); // newspaper
  book2.tostringNamePage();

}
